package com.postal.code.api.controller;

import com.postal.code.api.domain.PostalCode;
import com.postal.code.api.service.PostalCodeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/postal")
public class PostalCodeController {

    private final PostalCodeService postalCodeService;
    private final Validator validator;

    @PostMapping
    public PostalCode create(@RequestBody PostalCode postalCode) {
        this.validatePostalCode(postalCode);
        return postalCodeService.create(postalCode);
    }

    @PostMapping
    public List<PostalCode> createAll(@RequestBody List<PostalCode> postalCodes) {

        return postalCodes.stream()
                .peek(this::validatePostalCode)
                .map(this::create)
                .collect(Collectors.toList());
    }


    @GetMapping("/{pin}")
    public List<PostalCode> getAllRecentByPin(@PathVariable("pin") String pin) {
        return postalCodeService.getAllByPin(pin);
    }

    @GetMapping("/lat/{lat}/lang/{lang}")
    public PostalCode getByLatLang(@PathVariable("lat") Double lat, @PathVariable("lang") Double lang) {
        return postalCodeService.getByLatLang(lat, lang);
    }

    @GetMapping("/near/{pin}/maxDistance/{maxDistance}")
    public List<PostalCode> getNearByPostalCodesTo(@PathVariable("pin") String pin,
                                                   @PathVariable("maxDistance") Integer maxDistance) {
        return postalCodeService.getAllWithInRadiusFrom(maxDistance, pin);
    }

    private void validatePostalCode(PostalCode postalCode) {
        Set<ConstraintViolation<PostalCode>> exceptions = validator.validate(postalCode);
        if (!exceptions.isEmpty()) {
            throw new ConstraintViolationException(exceptions);
        }
    }
}
