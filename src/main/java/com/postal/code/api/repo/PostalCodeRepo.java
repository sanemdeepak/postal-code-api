package com.postal.code.api.repo;

import com.postal.code.api.domain.PostalCode;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@Repository
public interface PostalCodeRepo extends MongoRepository<PostalCode, UUID>, PostalCodeCustomRepo {
}
