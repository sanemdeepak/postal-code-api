package com.postal.code.api.repo;

import com.postal.code.api.domain.PostalCode;

import java.util.List;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
public interface PostalCodeCustomRepo {

    List<PostalCode> findAllRecentByPin(String pin);

    PostalCode findByLatLang(Double lat, Double lang);

    List<PostalCode> findAllWithInRadiusFrom(Integer radius, String pin);
}
