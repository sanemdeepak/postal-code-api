package com.postal.code.api.repo.impl;

import com.postal.code.api.domain.Coordinates;
import com.postal.code.api.domain.PostalCode;
import com.postal.code.api.exception.PostalCodeNotFound;
import com.postal.code.api.repo.PostalCodeCustomRepo;
import lombok.RequiredArgsConstructor;
import org.springframework.data.geo.Point;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@Repository
@RequiredArgsConstructor
public class PostalCodeCustomRepoImpl implements PostalCodeCustomRepo {
    private final MongoOperations mongoOperations;

    @Override
    public List<PostalCode> findAllRecentByPin(String pin) {
        Query query = Query.query(
                Criteria.where("createdOn")
                        .gt(Date.from(Instant.now().minus(30, ChronoUnit.DAYS)))
                        .lt(Date.from(Instant.now()))
                        .and("pin")
                        .is(pin))
                .limit(5);
        return mongoOperations.find(query, PostalCode.class);
    }

    @Override
    public PostalCode findByLatLang(Double lat, Double lang) {
        Point point = this.getPointFromLatLang(lat, lang);

        Query query = Query.query(Criteria
                .where("Location")
                .nearSphere(point)
                .maxDistance(100)
                .minDistance(0));

        return mongoOperations.find(query, PostalCode.class)
                .stream()
                .findAny()
                .orElseThrow(PostalCodeNotFound::new);
    }

    @Override
    public List<PostalCode> findAllWithInRadiusFrom(Integer radius, String pin) {
        PostalCode postalCode = findAllRecentByPin(pin)
                .stream()
                .findFirst()
                .orElseThrow(PostalCodeNotFound::new);

        Coordinates postalCodeCoordinates = Optional.of(postalCode.getLocation()).get().getCoordinates();

        Point point = this.getPointFromLatLang(postalCodeCoordinates.getLatitude(), postalCodeCoordinates.getLongitude());

        Query query = Query.query(Criteria
                .where("Location")
                .nearSphere(point)
                .maxDistance(radius)
                .minDistance(0)
                .and("pin")
                .ne(pin));
        return mongoOperations.find(query, PostalCode.class);
    }


    private Point getPointFromLatLang(Double lat, Double lang) {
        return new Point(lat, lang);
    }

}
