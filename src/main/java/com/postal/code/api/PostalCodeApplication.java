package com.postal.code.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@SpringBootApplication
public class PostalCodeApplication {
    public static void main(String[] args) {
        SpringApplication.run(PostalCodeApplication.class, args);
    }

    @Bean
    public Validator getValidator() {
        ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        return validatorFactory.getValidator();
    }
}
