package com.postal.code.api.service;

import com.postal.code.api.domain.PostalCode;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
public interface PostalCodeService {
    PostalCode create(PostalCode postalCode);

    PostalCode getById(UUID id);

    List<PostalCode> getAllByPin(String pin);

    PostalCode getByLatLang(Double lat, Double lang);

    List<PostalCode> getAllWithInRadiusFrom(Integer radius, String pin);

}
