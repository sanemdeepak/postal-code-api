package com.postal.code.api.service;

import com.postal.code.api.domain.PostalCode;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.Date;
import java.util.UUID;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@Component
public class PreProcessor {


    public PostalCode process(PostalCode postalCode) {
        postalCode.setId(UUID.randomUUID());
        postalCode.setCreatedOn(Date.from(Instant.now()));
        return postalCode;
    }
}
