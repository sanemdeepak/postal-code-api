package com.postal.code.api.service.impl;

import com.postal.code.api.domain.PostalCode;
import com.postal.code.api.exception.PostalCodeNotFound;
import com.postal.code.api.repo.PostalCodeRepo;
import com.postal.code.api.service.PostalCodeService;
import com.postal.code.api.service.PreProcessor;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@Service
@RequiredArgsConstructor
public class PostalCodeServiceImpl implements PostalCodeService {

    private final PostalCodeRepo postalCodeRepo;
    private final PreProcessor preProcessor;


    @Override
    public PostalCode create(PostalCode postalCode) {
        preProcessor.process(postalCode);
        return postalCodeRepo.insert(postalCode);
    }

    @Override
    public PostalCode getById(UUID id) {
        return postalCodeRepo.findById(id).orElseThrow(PostalCodeNotFound::new);
    }

    @Override
    public List<PostalCode> getAllByPin(String pin) {
        return postalCodeRepo.findAllRecentByPin(pin);
    }

    @Override
    public PostalCode getByLatLang(Double lat, Double lang) {
        return postalCodeRepo.findByLatLang(lat, lang);
    }

    @Override
    public List<PostalCode> getAllWithInRadiusFrom(Integer radius, String pin) {
        return postalCodeRepo.findAllWithInRadiusFrom(radius, pin);
    }
}
