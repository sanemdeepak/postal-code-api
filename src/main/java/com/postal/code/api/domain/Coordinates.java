package com.postal.code.api.domain;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@Data
public class Coordinates {
    @NotBlank
    private Double longitude;
    @NotBlank
    private Double latitude;
}
