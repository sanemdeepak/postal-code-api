package com.postal.code.api.domain;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexType;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.UUID;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@Data
@Document(collection = "PostalCode")
public class PostalCode {

    @GeoSpatialIndexed(type = GeoSpatialIndexType.GEO_2DSPHERE)
    @NotNull
    Location location;

    @Id
    private UUID id;

    @Indexed
    @NotBlank
    private String pin;

    private String locality;

    @Indexed
    private Date createdOn;
}
