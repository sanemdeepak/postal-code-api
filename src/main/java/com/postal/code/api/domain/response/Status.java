package com.postal.code.api.domain.response;

/**
 * Created by sanemdeepak on 12/9/18.
 */
public enum Status {
    SUCCESS,
    FAIL,
    ERROR
}
