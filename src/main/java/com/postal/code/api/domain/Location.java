package com.postal.code.api.domain;

import lombok.Data;

import javax.validation.Valid;

/**
 * Created by sanemdeepak on 2018-12-16.
 */
@Data
public class Location {
    private final String type = "Point";
    @Valid
    private Coordinates coordinates;
}
