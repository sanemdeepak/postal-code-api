package com.postal.code.api.repo;

import com.postal.code.api.domain.PostalCode;
import com.postal.code.api.repo.impl.PostalCodeCustomRepoImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;

import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PostalCodeCustomRepoTest {

    @Mock
    private MongoOperations mongoOperations;

    @InjectMocks
    private PostalCodeCustomRepoImpl postalCodeCustomRepo;


    @Before
    public void setUp() {
    }

    @Test
    public void findAllRecentByPinHappyPath() {
        String pin = UUID.randomUUID().toString();

        List postalCodes = Collections.singletonList(new PostalCode());
        when(mongoOperations.find(any(Query.class), any())).thenReturn(postalCodes);

        postalCodeCustomRepo.findAllRecentByPin(pin);

        verify(mongoOperations).find(any(), any());
    }
}